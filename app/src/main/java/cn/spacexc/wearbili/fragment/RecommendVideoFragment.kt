package cn.spacexc.wearbili.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.OnScrollChangeListener
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.wear.compose.material.ScalingLazyColumn
import androidx.wear.compose.material.Text
import cn.spacexc.wearbili.databinding.FragmentProfileBinding
import cn.spacexc.wearbili.databinding.FragmentRecommendVideoBinding
import cn.spacexc.wearbili.dataclass.video.rcmd.app.RecommendVideo
import cn.spacexc.wearbili.manager.SettingsManager
import cn.spacexc.wearbili.manager.VideoManager
import cn.spacexc.wearbili.ui.ModifierExtends.clickVfx
import cn.spacexc.wearbili.ui.VideoUis
import cn.spacexc.wearbili.utils.NumberUtils.toShortChinese
import cn.spacexc.wearbili.utils.ToastUtils
import cn.spacexc.wearbili.utils.VideoUtils
import cn.spacexc.wearbili.viewmodel.RecommendViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.google.gson.Gson
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException


class RecommendVideoFragment : Fragment() {
    private var _binding: FragmentRecommendVideoBinding? = null
    private val binding get() = _binding!!
    private val videoCardRows = ArrayList<VideoUis.VideoCardLayout.VideoCardDetail>()
    private val videoCardAdapter =
        VideoUis.VideoCardAdapter(videoCardRows)

    private var isLoading = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRecommendVideoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // recycler view
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recommendVideoRecyclerView.layoutManager = layoutManager
        binding.recommendVideoRecyclerView.adapter = videoCardAdapter

        // swipe refresh
        binding.swipeRefreshLayout.setOnRefreshListener {
            refresh(true)
        }

        binding.errorPage.setOnClickListener {
            refresh()
        }

        binding.recommendVideoRecyclerView.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val position = layoutManager.findLastVisibleItemPosition()
                if (position != RecyclerView.NO_POSITION && position == videoCardAdapter.itemCount - 1) {
                    refresh()
                }
            }
        })

        refresh()
    }

    fun switchPage(success: Boolean = true) {
        if (success || videoCardRows.any()) {
            binding.errorPage.visibility = GONE
            binding.swipeRefreshLayout.visibility = VISIBLE
        } else {
            binding.errorPage.visibility = VISIBLE
            binding.swipeRefreshLayout.visibility = GONE
        }
    }

    fun refresh(clear: Boolean = false) {
        if (isLoading) return

        if (clear) videoCardRows.clear()

        binding.swipeRefreshLayout.isRefreshing = true
        isLoading = true

        VideoManager.getRecommendVideo(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                MainScope().launch {
                    ToastUtils.showText("网络异常")
                    switchPage(false)
                }
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(call: Call, response: Response) {
                val str = response.body?.string()
                try {
                    val videos = Gson().fromJson(str, RecommendVideo::class.java)

                    videoCardRows.addAll(videos.data.items.map {
                        VideoUis.VideoCardLayout.VideoCardDetail().apply {
                            videoName = it.title
                            coverUrl = it.cover ?: ""
                            views = it.cover_left_text_2 ?: ""
                            uploader = it.args.up_name ?: ""
                            hasViews = true
                            clickable = true
                            videoBvid = it.bvid ?: VideoUtils.av2bv("av${it.param}")
                            isBangumi = false
                            epid = it.param
                            badge = it.badge ?: ""
                        }
                    })

                    MainScope().launch {
                        if (videos.code == 0) {
                            val startPos = videoCardRows.size

                            if (clear) videoCardAdapter.notifyDataSetChanged()
                            else videoCardAdapter.notifyItemRangeInserted(
                                startPos,
                                videos.data.items.size
                            )

                            switchPage()
                        } else {
                            ToastUtils.makeText(
                                "${videos.code}: ${videos.message}"
                            ).show()
                            switchPage(false)
                        }
                    }
                } catch (_: Exception) {
                    MainScope().launch {
                        ToastUtils.makeText(
                            "加载失败"
                        ).show()
                        switchPage(false)
                    }
                } finally {
                    MainScope().launch {
                        binding.swipeRefreshLayout.isRefreshing = false
                        isLoading = false
                    }
                }
            }
        })
    }
}