package cn.spacexc.wearbili.utils

import android.view.View
import androidx.recyclerview.widget.RecyclerView.ViewHolder

class BindingViewHolder<TBinding : androidx.viewbinding.ViewBinding> : ViewHolder {
    private val _binding: TBinding;
    public val binding get() = _binding;
    private val _view: View;
    public val view get() = _view;
    public val context get() = _view.context

    constructor(itemView: View, itemBinding: TBinding) : super(itemView) {
        _view = itemView
        _binding = itemBinding
    }

    constructor(itemBinding: TBinding) : this(itemBinding.root, itemBinding)
}