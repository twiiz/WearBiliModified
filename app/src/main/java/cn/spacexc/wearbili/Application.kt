/*
SAY NO TO SUICIDE PUBLIC LICENSE

Version 1.0, September 2019

Copyright (C) 2022 XC-Qan

Everyone is permitted to copy and distribute verbatim copies
of this license document.

TERMS AND CONDITIONS FOR USE, REPRODUCTION, MODIFICATION, AND DISTRIBUTION

  1. You can do anything with the original copy,
  whenever, whatever, no limitation.

  2. When you are in despair, just talk to someone you trust,
  someone you love. Getting help from your family, your friends,
  the police, the community, the public.

  3. Keep yourself alive and say no to suicide.
*/

/* WearBili  Copyright (C) 2022  XC
This program is licensed under GPL Licence v3.
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it under certain conditions.
*/

package cn.spacexc.wearbili

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import cn.spacexc.wearbili.utils.ToastUtils
import com.google.gson.stream.JsonReader

/**
 * Created by XC-Qan on 2022/6/7.
 * I'm very cute so please be nice to my code!
 * 给！爷！写！注！释！
 * 给！爷！写！注！释！
 * 给！爷！写！注！释！
 */

@SuppressLint("StaticFieldLeak")
class Application : android.app.Application() {
    companion object {
        var context: Application? = null
        fun getContext(): Context {
            return context!!
        }

        fun getTag(): String {
            return "WearBiliTag"
        }

        const val TAG = "WearBiliTag"
    }

    override fun onCreate() {
        super.onCreate()

        Log.d(
            TAG, "\n" +
                    "SAY NO TO SUICIDE PUBLIC LICENSE \n" +
                    "\n" +
                    "Version 1.0, September 2019\n" +
                    "\n" +
                    "Copyright (C) 2022 XC-Qan\n" +
                    "\n" +
                    "Everyone is permitted to copy and distribute verbatim copies\n" +
                    "of this license document.\n" +
                    "\n" +
                    "TERMS AND CONDITIONS FOR USE, REPRODUCTION, MODIFICATION, AND DISTRIBUTION\n" +
                    "\n" +
                    "  1. You can do anything with the original copy, \n" +
                    "  whenever, whatever, no limitation.\n" +
                    "  \n" +
                    "  2. When you are in despair, just talk to someone you trust, \n" +
                    "  someone you love. Getting help from your family, your friends, \n" +
                    "  the police, the community, the public.\n" +
                    "  \n" +
                    "  3. Keep yourself alive and say no to suicide.\n" +
                    "\n"
        )
        Log.d(
            TAG,
            "别那么消极捏"
        )
        context = this
    }

    override fun onTerminate() {
        super.onTerminate()
        ToastUtils.makeText("onTerminate").show()
    }
}